from math import atan, pi, degrees
def rect_to_polar(a,b,rad):
    a, b = float(a), float(b)
    r = (a**2+b**2)**(1/2)
    theta = atan(b/a)
    return [r,theta] if rad == True else [r,degrees(theta)]

print rect_to_polar(4,5,True)
print rect_to_polar(1,1,False)
print rect_to_polar(2,1,False)

from math import exp, log

#From the plotting problems on Tuesday and Wednesday
def coolExponentialFunc(t):
    if t >= 10:
        return 7.38905609893065
    elif t <= 0:
        return ((3.0/10.0)*t+1)
    else:
        return exp(0.2*t)
def g(x):
    if x <= 0:
        return x*0
    elif (x > 0) and (x<=5) :
        return x**2
    else:
        return 40 - 3*x
def pyramid(x):
    if x > 0 and x<2:
        return 7
    elif x <= 0:
        return 2*x+7
    else:
        return 11-2*x

#From the airplane problem on Thursday
def airPlaneDist(t):
    if t <=0: return 0
    return (0.1*t-2.5) if t >= 50 else (0.001*t**2)
def airPlaneFuelEff(s):
    return 45*exp(-0.01*s)
def getPlaneSpeed(t):
    return doDiff(airPlaneDist, t, 0.1, 'c')*3600.0
def fuel_burn_rate(t):
    return getPlaneSpeed(t)/airPlaneFuelEff(getPlaneSpeed(t))
def doDiff(f, x, h, mode = None):
    def _centralDiff(f,x,h):
        return (f(x+h/2.0)-f(x-h/2.0))/(h)
    def _forwardDiff(f,x,h):
        return (f(x+h)-f(x))/h
    def _backwardDiff(f,x,h):
        return (f(x)-f(x-h))/h
    if mode == "c":
        return _centralDiff(f, x, h)
    elif mode == "f":
        return _forwardDiff(f,x,h)
    elif mode == "b":
        return _backwardDiff(f,x,h)
    else:
        print "Please be decisive"

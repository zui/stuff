def totalTax(income):
    taxBracket = [0, 9075.0, 36900.0, 89350.0, 186350.0,405100.0,406650.0]
    if income > 0 and income <= taxBracket[1]:
        return 0.1*income
    elif income > taxBracket[1] and income <= taxBracket[2]:
        return taxBracket[1]*0.1 + (income - taxBracket[1])*0.15
    elif income > taxBracket[2] and income <= taxBracket[3]:
        return taxBracket[1]*0.1 + (taxBracket[2]-taxBracket[1])*0.15 + (income-taxBracket[2])*0.25
    elif income > taxBracket[3] and income <= taxBracket[4]:
        return taxBracket[1]*0.1 + (taxBracket[2]-taxBracket[1])*0.15 + (taxBracket[3]-taxBracket[2])*0.25 + (income-taxBracket[3])*0.28
    elif income > taxBracket[4] and income <= taxBracket[5]:
        return taxBracket[1]*0.1 + (taxBracket[2]-taxBracket[1])*0.15 + (taxBracket[3]-taxBracket[2])*0.25 + (taxBracket[4]-taxBracket[3])*0.28 + (income-taxBracket[4])*0.33
    elif income > taxBracket[5] and income <= taxBracket[6]:
        return taxBracket[1]*0.1 + (taxBracket[2]-taxBracket[1])*0.15 + (taxBracket[3]-taxBracket[2])*0.25 + (taxBracket[4]-taxBracket[3])*0.28 + (taxBracket[5]-taxBracket[4])*0.33 + (income-taxBracket[5])*0.35
    elif income > taxBracket[6]:
        return taxBracket[1]*0.1 + (taxBracket[2]-taxBracket[1])*0.15 + (taxBracket[3]-taxBracket[2])*0.25 + (taxBracket[4]-taxBracket[3])*0.28 + (taxBracket[5]-taxBracket[4])*0.33 + (taxBracket[6]-taxBracket[5])*0.35 + (income-taxBracket[6])*0.396
    else:
        return 0
def effective_tax_rate(income):
    return float(totalTax(income))/float(income)

print effective_tax_rate(72603)
print effective_tax_rate(67782)
print effective_tax_rate(6886)
print effective_tax_rate(77925)
print effective_tax_rate(5829)
print effective_tax_rate(8826160)
print effective_tax_rate(9726468)
print effective_tax_rate(782703)
print effective_tax_rate(3996530)
print effective_tax_rate(4942940)
print effective_tax_rate(6873726)
print effective_tax_rate(9437601)
print effective_tax_rate(1393046)
print effective_tax_rate(1559485)
print effective_tax_rate(5718612)

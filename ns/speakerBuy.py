def getRealCost(n):
    if n > 0 and n < 10:
        return n*4.15
    elif n >= 10 and n< 100:
        return n*3.432
    elif n>=100 and n <500:
        return n*2.574
    elif n>=500 and n <1000:
        return n*2.145
    elif n>=1000 and n <2500:
        return n*1.90190
    elif n >= 2500:
        return n*1.82325
    else:
        return 0
def costFinder(n):
    result=[]
    for i in range(2600):
        result.append((n+i, getRealCost(n+i)))
    result.sort(key=lambda tup: tup[1])
    #print result if n == 900 or n == 950 else None
    return result[0] if result[0][1] <= getRealCost(n) else (n, getRealCost(n))

print costFinder(900)
print costFinder(950)
print costFinder(0)
print costFinder(-1)
print costFinder(2498)
print costFinder(10000)

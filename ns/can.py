pi = 3.14159
rate=4
d=.5
max_vol=290000
max_height = 1.5 #290,000 ml

def conv_ml_2_hgt(ml,d):
    #Take amount of water and convert it to the height of the can in meter
    area = ((0.5*d)**2)*pi
    return (ml/(1e6))/area
def conv_hgt_2_ml(hgt,d):
    #Inverse of conv_ml_2_hgt
    area = ((0.5*d)**2)*pi
    return area*hgt*1e6

def leak_rate(t):
    int_t = conv_hgt_2_ml(fill_level(t),d)
    sec_t = conv_hgt_2_ml(fill_level(t+0.00000001),d)
    return round((sec_t - int_t)/0.00000001,3)

def fill_level(t):
    #Main func
    if t <= 0:
        return 0
    elif (t>0) and ((t*rate)<max_vol):
        return conv_ml_2_hgt((t*rate),d)
    else:
        return max_height

    
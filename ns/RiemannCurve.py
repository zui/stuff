from math import fabs
def area_left(f,x_lo,x_hi,slice_number):
    x_lo,x_hi,slice_number= float(x_lo),float(x_hi),float(slice_number)
    interval = float(fabs(x_hi-x_lo)/(slice_number))
    print interval, [f(x_lo+x*interval) for x in range(int(slice_number))]
    return sum([f(x_lo+x*interval) for x in range(int(slice_number))])*interval

'''
def area_left(f,x_lo,x_hi,slice_number):
    interval = float(fabs(x_hi-x_lo)/(slice_number))
    print interval
    result = []
    #result.append(f(x_lo))
    for i in range(slice_number):
        result.append(f(x_lo+i*interval))
    print result
    return sum(result)*interval
'''
def area_right(f,x_lo,x_hi,slice_number):
    x_lo,x_hi,slice_number= float(x_lo),float(x_hi),float(slice_number)
    interval = float(fabs(x_hi-x_lo)/(slice_number))
    return sum([f(x_lo+x*interval) for x in range(1,int(slice_number+1))])*interval
'''
def area_center(f,x_lo,x_hi,slice_number):
    x_lo,x_hi,slice_number= float(x_lo),float(x_hi),float(slice_number)
    interval = float(fabs(x_hi-x_lo)/(slice_number))
    return sum([(i+float(fabs((x_hi-x_lo)/(2*slice_number)))) for i in [ f(x_lo+x*interval) for x in range(int(slice_number))]])*interval
'''
def area_center(f,x_lo,x_hi,slice_number):
    x_hi, x_lo = max(x_lo, x_hi), min(x_lo,x_hi)
    interval = float(fabs(x_hi-x_lo)/(slice_number))
    #print interval
    result = []
    #result.append(f(x_lo))
    for i in range(slice_number):
        result.append(f((x_lo+(interval/2))+i*interval))
    print result
    return sum(result)*interval


print area_center(lambda x: 5*x, -5, 5 ,1)
print area_center(lambda x: 5*x, -5, 5 ,2)

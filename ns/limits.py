from math import log, exp, sin, cos

def crazyLimit(x):
  first = (-1)*(x**(1/5))
  second = exp(x)*(1/(1+log(x)))
  third = sin(x)*cos(x)
  print first, second, third
  return first + second + third

print crazyLimit(3)
#crazyLimit(0)
#crazyLimit(1/exp(1))

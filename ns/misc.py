def mult(m, n):
    if n == 0: return 0
    else:
        return m if n == 1 else m + mult(m, n-1)
def posNegMult(m, n):
    if n == 0: return 0
    elif n < 0:
        result = m if abs(n) == 1 else m + mult(m, abs(n)-1)
        return -result
    else:
        return m if n == 1 else m + mult(m, n -1)
def isOdd(x):
    return False if x%2 == 0 else True
def mod(m,n):
    t = int(m/n)
    return m-(n*t)

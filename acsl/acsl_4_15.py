#Duy Nguyen - 4th ACSL - Something about the Quine-McClusky Algorithm

#Modify the input array to run
inputArray = [
		[4, 8, 9, 10, 11, 12, 14, 15, -1],
		[4, 9],
		[4, 8],
		[8, 4, 10, 11],
		[8,10,12,14],
		[10,11,14,15]
]

def getBin(ints):
#Turn an interget into binary with 4 bit list
	result = str(bin(ints))[2:]
	resultList = []
	while (len(result)<4):
		result = "0" + result
	for i in result:
		resultList.append(i)
	return resultList
def compBins(bin1,bin2):
#Compare two 4-list binary
	result = []
	for i in range(0,len(bin1)):
		if bin1[i] == bin2[i]:
			result.append(bin1[i])
		else:
			result.append("x")
	return "".join(result)
def words(bint):
#Produce the Bool expression
	returnList = []
	alphabet = ["a","b","c","d"]
	for i in range(0,len(bint)):
		if bint[i] == "0":
			returnList.append(alphabet[i])
		elif bint[i] == "1":
			returnList.append(alphabet[i].upper())
	return "".join(returnList)
def incCheck(bint):
#Check if the request is vaild
	if bint in inputArray[0]:
		return True
def intComp1(int1,int2):
#Wrapper
	if compBins(getBin(int1),getBin(int2)).count("x") > 2:
		#Protect against two number differ by 3 digits
		return None
	if ((getBin(int1).count("x") == 0) or (getBin(int1).count("x") == 0)):
		#Protect against two number in same index
		if (compBins(getBin(int1),getBin(int2)).count("x") > 1):
			return None
	if (incCheck(int1) and incCheck(int2)):
		#Protect against number that not in term or doesn't give result
		if (compBins(getBin(int1),getBin(int2)) != None):
			return (compBins(getBin(int1),getBin(int2)),
					words(compBins(getBin(int1),getBin(int2))))
	else:
		return None
def intComp2(ints):
#Wrapper for quadruples
	if len(set(ints)) != 4:
		return None
	if ((intComp1(ints[0],ints[1]) != None) and (intComp1(ints[2],ints[3]) != None)):
		return (compBins(intComp1(ints[0],ints[1])[0],intComp1(ints[2],ints[3])[0]),
				words(compBins(intComp1(ints[0],ints[1])[0],intComp1(ints[2],ints[3])[0])))
	else:
		return None
#Let's run this thing
if (intComp1(inputArray[1][0],inputArray[1][1])) != None:
    print (", ".join(intComp1(inputArray[1][0],inputArray[1][1])))
else:
    print ("NONE")
if (intComp1(inputArray[2][0],inputArray[2][1])) != None:
    print (", ".join(intComp1(inputArray[2][0],inputArray[2][1])))
else:
    print ("NONE")
if (intComp2(inputArray[3])) != None:
    print (", ".join(intComp2(inputArray[3])))
else:
    print ("NONE")
if (intComp2(inputArray[4])) != None:
    print (", ".join(intComp2(inputArray[4])))
else:
    print ("NONE")
if (intComp2(inputArray[5])) != None:
    print (", ".join(intComp2(inputArray[5])))
else:
    print ("NONE")



